<?php
function genId($str, $length){
    function map16to64($three_char){
        $ciphers = str_split(
            '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_');
        $num = intval($three_char, 16);
        return $ciphers[$num / 64] . $ciphers[$num % 64];
    }
    $id = implode( array_map( "map16to64", str_split(md5($str), 3) ) );
    return substr($id, 0, $length);
}
