<?php
require_once 'includes/id.php';

$config = include('includes/config.php');

try {
    $dbh = new PDO($config["pdo-dsn"], $config["db-user"], $config["db-password"], [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    ]);

    $url_id = $url = $_REQUEST["url"];
    $is_new_url = true;
    $query = $dbh->prepare("SELECT url FROM url_map WHERE id = :id");
    do {
        $url_id = genId($url_id, $config["url-id-len"]);
        $query->execute([":id" => $url_id]);
        $res = $query->fetch();
    } while ($res && $is_new_url = $res["url"] !== $url);

    if($is_new_url){
        $query = $dbh->prepare("INSERT INTO url_map (id, url) VALUES (:id, :url)");
        $query->execute([":id" => $url_id, ":url" => $url]);
    }
    echo json_encode( [
        "id"    => $url_id,
        "url"   => $url,
        "error" => null
    ]);
} catch (PDOException $e) {
    echo json_encode([
        "error" => "database error",
    ]);
}
