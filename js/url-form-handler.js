$( document ).ready(function() {
  $("#url_form_btn").click(
    function(){
      $("#result_form").html("Отправка запроса");
      jQuery.ajax({
        url:      'addurl.php',
        type:     "POST",
        dataType: "html",
        data: jQuery("#url_form").serialize(),
        success: function(response) {
          result = jQuery.parseJSON(response);
          if(!result.error){
            $("#result_form").html(window.location.hostname+"/"+result.id);
          } else {
            $("#result_form").html(result.error);
          }
        },
        error: function(response) {
          $("#result_form").html("Ошибка запроса");
        }
      });
      return false;
    }
  );
});
