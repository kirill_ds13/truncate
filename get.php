<?php
$config = include('includes/config.php');
if (isset($_REQUEST["key"]) && strlen($_REQUEST["key"]) === $config["url-id-len"]) {
    $url_id = $_REQUEST["key"];
    try {
        $dbh = new PDO($config["pdo-dsn"], $config["db-user"], $config["db-password"], [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ]);
        $query = $dbh->prepare("SELECT url FROM url_map WHERE id = :id");
        $query->execute([":id" => $url_id]);
        if($res = $query->fetch()){
            $url = $res["url"];
            header("Location:".$url);
        } else {
            header("Location: index.html");
        }
    } catch (PDOException $e) {
        echo "Извините, что-то сломалось :(";
    }
} else {
    header("Location: index.html");
}
